# Build

```bash
cd imageproc9
go mod tidy
cd imageproc9
go build
```

# Run

### Classic sharpen kernel
```bash
./imageproc9 cast -t sharpen samples/sample1.png samples/sample2.png samples/sample3.png
```

### Test alpha kernel
```bash
./imageproc9 cast -t testalpha -a 0.0 samples/sample1.png samples/sample2.png samples/sample3.png
```


### Decay
```bash
./imageproc9 cast -t decay samples/sample6_out.png samples/sample4_out.png samples/sample5_out.png
```
| Original              | Decayed                   |
|-----------------------|---------------------------|
| ![b](doc/sample4.png) | ![a](doc/sample4_out.png) |

| Original                | Decayed                     |
|-----------------------|---------------------------|
| ![b](doc/sample5.png) | ![a](doc/sample5_out.png) |

| Original                | Decayed                     |
|-----------------------|---------------------------|
| ![b](doc/sample6.png) | ![a](doc/sample6_out.png) |