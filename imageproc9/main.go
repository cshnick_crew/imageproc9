package main

import (
	"cshnick/imageproc9/cast"
	"cshnick/imageproc9/mainhlp"
	"flag"
	"image"
	"log"
)

func main() {
	var (
		args *mainhlp.CmdArgs
		err  error
	)
	log.SetFlags(0)
	log.SetOutput(new(mainhlp.LogWriter))
	if args, err = mainhlp.ReadCmdArgs(); err != nil {
		flag.Usage()
		log.Fatalf("error: %s", err)
	}
	switch args.Cmd {
	case mainhlp.CastKey:
		log.Printf("----------------------------------------")
		for _, file := range args.Cast.Files {
			var img *image.Image
			if img, err = cast.ReadImg(file); err != nil {
				log.Fatalf("error: %s", err)
			}
			var result *image.RGBA
			log.Printf("Starting image conversion for %q", file)
			log.Printf("Selected kernel: %s", args.Cast.Transformation)
			switch args.Cast.Transformation {
			case "testalpha":
				log.Printf("Selected alpha: %f", args.Cast.Alpha)
				result, err = cast.SharpenTestWithAlpha(img, args.Cast.Alpha)
			case "sharpen":
				result, err = cast.ClassicSharpen(img)
			case "decay":
				result = cast.DecayImage(img)
			}
			if err != nil {
				log.Printf("%s", err)
			}
			log.Printf("Writing out...")
			if err = cast.WriteOutImage(file, args.Cast.Transformation, result); err != nil {
				log.Printf("%s", err)
				continue
			}
			log.Printf("Done")
		}
		log.Printf("----------------------------------------")
		log.Printf("Completed image processing, files have been uploaded to out/%s/", args.Cast.Transformation)
	}
}
