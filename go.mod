module cshnick/imageproc9

go 1.16

require (
	github.com/clagraff/argparse v1.0.1
	github.com/nsf/termbox-go v1.1.1 // indirect
)
