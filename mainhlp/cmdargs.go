package mainhlp

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
)

const (
	CastKey = "cast"
)

type CmdArgs struct {
	Cmd  string
	Cast struct {
		Transformation string
		Files          []string
		Alpha          float64
	}
}

func ensureFiles(files []string) error {
	if len(files) == 0 {
		return errors.New("need at least one positional argument")
	}
	for _, file := range flag.Args() {
		if fi, err := os.Stat(file); err != nil || fi.IsDir() {
			return errors.New(fmt.Sprintf("%s must be a file", file))
		}
		file, err := os.Open(file)
		defer func(file *os.File) {
			err := file.Close()
			if err != nil {
				log.Fatalf("Could not close the file")
			}
		}(file)
		if err != nil {
			return err
		}
		buff := make([]byte, 512)
		if _, err = file.Read(buff); err != nil {
			return err
		}
		ct := http.DetectContentType(buff)
		if !strings.HasPrefix(ct, "image/") {
			return errors.New(fmt.Sprintf("%s must be an image", file))
		}
	}
	return nil
}

func ReadCmdArgs() (*CmdArgs, error) {
	result := &CmdArgs{}
	if len(os.Args) < 2 {
		return nil, errors.New("subcommand must be specified: gray|CastKey")
	}
	switch os.Args[1] {
	case CastKey:
		result.Cmd = CastKey
		castSub := flag.NewFlagSet("CastKey", flag.ExitOnError)
		comments := map[string]string{
			"t": "Transformation type. Only 'sharpen' is currently supported",
			"a": "Alpha value, used to parametrize test kernel",
		}
		castSub.StringVar(&result.Cast.Transformation, "t", "sharpen", comments["t"])
		castSub.Float64Var(&result.Cast.Alpha, "a", -255, comments["a"])
		if err := castSub.Parse(os.Args[2:]); err != nil {
			return nil, err
		}
		if err := ensureFiles(castSub.Args()); err != nil {
			return nil, err
		}
		if result.Cast.Transformation == "testalpha" && result.Cast.Alpha == -255 {
			return nil, errors.New("'alpha' must be specified via '-a'")
		}
		result.Cast.Files = castSub.Args()
	default:
		return nil, errors.New("subcommand must be specified: gray|CastKey")
	}
	return result, nil
}
