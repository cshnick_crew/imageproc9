package cast

import (
	"errors"
	"image"
)

type CanvasExtender interface {
	CreateExtendCanvas() (*image.RGBA, error)
}

type PaddingBounds struct {
	Top, Left, Bottom, Right int
}

type ReflectPaddingRGBA struct {
	Src        *image.RGBA
	KernelSize image.Point
	Anchor     image.Point
}

func (rpr *ReflectPaddingRGBA) CreateExtendCanvas() (*image.RGBA, error) {
	if rpr.KernelSize.X < 0 || rpr.KernelSize.Y < 0 {
		return nil, errors.New("negative kernel size")
	}
	if rpr.Anchor.X < 0 || rpr.Anchor.Y < 0 {
		return nil, errors.New("anchor should be a positive point")
	}
	if rpr.Anchor.X > rpr.KernelSize.X || rpr.Anchor.Y > rpr.KernelSize.Y {
		return nil, errors.New("anchor has to be within kernel")
	}
	// Calculat padding bounds
	var bounds = PaddingBounds{
		Left:   rpr.Anchor.X,
		Right:  rpr.KernelSize.X - rpr.Anchor.X - 1,
		Top:    rpr.Anchor.Y,
		Bottom: rpr.KernelSize.Y - rpr.Anchor.Y - 1,
	}
	// Original img size
	isz := rpr.Src.Bounds().Size()
	eWidth := bounds.Left + bounds.Right + isz.X
	eHeight := bounds.Top + bounds.Bottom + isz.Y
	// Rect from outer bounds
	extendedRect := image.Rect(0, 0, eWidth, eHeight)
	result := image.NewRGBA(extendedRect)
	//Extend canvas top left
	for x := bounds.Left; x < isz.X+bounds.Left; x++ {
		for y := bounds.Top; y < isz.Y+bounds.Top; y++ {
			result.Set(x, y, rpr.Src.RGBAAt(x-bounds.Left, y-bounds.Top))
		}
	}
	// Reflect top
	for x := bounds.Left; x < isz.X+bounds.Left; x++ {
		for y := 0; y < bounds.Top; y++ {
			pixel := rpr.Src.At(x-bounds.Left, bounds.Top-y)
			result.Set(x, y, pixel)
		}
	}
	// Reflect bottom
	for x := bounds.Left; x < isz.X+bounds.Left; x++ {
		for y := bounds.Top + isz.Y; y < isz.Y+bounds.Top+bounds.Bottom; y++ {
			pixel := rpr.Src.At(x-bounds.Left, isz.Y-(y-bounds.Top-isz.Y)-2)
			result.Set(x, y, pixel)
		}
	}
	// Reflect left
	for y := 0; y < isz.Y+bounds.Bottom+bounds.Top; y++ {
		for x := 0; x < bounds.Left; x++ {
			pixel := result.At(2*bounds.Left-x, y)
			result.Set(x, y, pixel)
		}
	}
	// Reflect right
	for y := 0; y < isz.Y+bounds.Bottom+bounds.Top; y++ {
		for x := isz.X + bounds.Left; x < isz.X+bounds.Left+bounds.Right; x++ {
			pixel := result.At(isz.X+bounds.Left-(x-isz.X-bounds.Left)-2, y)
			result.Set(x, y, pixel)
		}
	}
	return result, nil
}

func clamp(value float64, min float64, max float64) float64 {
	if value < min {
		return min
	} else if value > max {
		return max
	}
	return value
}
