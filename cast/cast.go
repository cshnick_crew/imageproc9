package cast

import (
	"image"
	"image/color"
)

const FUINT8MIN float64 = 0
const FUINT8MAX = float64(^uint8(0))

var ClassicSharpenKernel = Kernel{
	Content: [][]float64{
		{-0.0, -1.0, -0.0},
		{-1.0, +5.0, -1.0},
		{-0.0, -1.0, -0.0},
	},
	Width:  3,
	Height: 3,
}

func NewTestKernelWithAlpha(af float64) *Kernel {
	kn := &Kernel{
		Content: [][]float64{
			{+(af + 1), -(af / 8), -(af / 8)},
			{-(af / 8), -(af / 8), -(af / 8)},
			{-(af / 8), -(af / 8), -(af / 8)},
		},
		Width:  3,
		Height: 3,
	}
	return kn
}

func NewDecayKernel() *Kernel {
	kn := &Kernel{
		Content: [][]float64{
			{0.393, 0.769, 0.189},
			{0.349, 0.686, 0.168},
			{0.272, 0.534, 0.131},
		},
		Width:  3,
		Height: 3,
	}
	return kn
}

func DecayImage(img *image.Image) *image.RGBA {
	var result *image.RGBA
	kn := NewDecayKernel()
	rgba := ToRgba(img)
	result = image.NewRGBA(rgba.Bounds())
	originalSize := rgba.Bounds().Size()
	RunParallel(originalSize, func(x int, y int) {
		pixel := rgba.RGBAAt(x, y)

		newR := float64(pixel.R)*kn.At(0, 0) + float64(pixel.G)*kn.At(0, 1) + float64(pixel.B)*kn.At(0, 2)
		newG := float64(pixel.R)*kn.At(1, 0) + float64(pixel.G)*kn.At(1, 1) + float64(pixel.B)*kn.At(1, 2)
		newB := float64(pixel.R)*kn.At(2, 0) + float64(pixel.G)*kn.At(2, 1) + float64(pixel.B)*kn.At(2, 2)

		newR = clamp(newR, FUINT8MIN, FUINT8MAX)
		newG = clamp(newG, FUINT8MIN, FUINT8MAX)
		newB = clamp(newB, FUINT8MIN, FUINT8MAX)

		result.Set(x, y, color.RGBA{
			R: uint8(newR),
			G: uint8(newG),
			B: uint8(newB),
			A: pixel.A,
		})
	})
	return result
}

func ConvolveRGBA(img *image.RGBA, kernel *Kernel, anchor image.Point) (*image.RGBA, error) {
	kernelSize := kernel.Size()
	rpr := ReflectPaddingRGBA{
		Src:        img,
		Anchor:     anchor,
		KernelSize: kernelSize,
	}
	extended, err := rpr.CreateExtendCanvas()
	if err != nil {
		return nil, err
	}
	originalSize := img.Bounds().Size()
	result := image.NewRGBA(img.Bounds())
	RunParallel(originalSize, func(x int, y int) {
		sumR, sumG, sumB := 0.0, 0.0, 0.0
		for kx := 0; kx < kernelSize.X; kx++ {
			for ky := 0; ky < kernelSize.Y; ky++ {
				pixel := extended.RGBAAt(x+kx, y+ky)
				sumR += float64(pixel.R) * kernel.At(kx, ky)
				sumG += float64(pixel.G) * kernel.At(kx, ky)
				sumB += float64(pixel.B) * kernel.At(kx, ky)
			}
		}
		sumR = clamp(sumR, FUINT8MIN, FUINT8MAX)
		sumG = clamp(sumG, FUINT8MIN, FUINT8MAX)
		sumB = clamp(sumB, FUINT8MIN, FUINT8MAX)
		rgba := img.RGBAAt(x, y)
		result.Set(x, y, color.RGBA{
			R: uint8(sumR),
			G: uint8(sumG),
			B: uint8(sumB),
			A: rgba.A,
		})
	})
	return result, nil
}
