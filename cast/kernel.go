package cast

import (
	"image"
)

type Mx interface {
	At(x, y int) float64
}

type Kernel struct {
	Content [][]float64
	Width   int
	Height  int
}

func (k *Kernel) At(x, y int) float64 {
	return k.Content[x][y]
}

func (k *Kernel) Set(x int, y int, value float64) {
	k.Content[x][y] = value
}

func (k *Kernel) Size() image.Point {
	return image.Point{X: k.Width, Y: k.Height}
}
