package cast

import (
	"errors"
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"log"
	"os"
	"path/filepath"
)

func ReadImg(file string) (*image.Image, error) {
	fp, err := os.Open(file)
	defer func(fp *os.File) {
		err := fp.Close()
		if err != nil {
			log.Printf("error closing file %s", file)
		}
	}(fp)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Error reading file %s. Maybe corrupted", file))
	}
	var img image.Image
	img, _, err = image.Decode(fp)
	if err != nil {
		log.Fatalf("Error reading file %s. Maybe corrupted", file)
	}
	return &img, nil
}

func WriteOutImage(file string, transformation string, img image.Image) error {
	abspath, err := filepath.Abs("./out")
	if err != nil {
		return err
	}
	abspath = filepath.Join(abspath, transformation)
	if err := os.MkdirAll(abspath, 0755); err != nil {
		return err
	}
	newFileName := Basename(file, "png")
	newFileName = filepath.Join(abspath, newFileName)
	var f *os.File
	f, err = os.OpenFile(newFileName, os.O_WRONLY, 0644)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			f, err = os.OpenFile(newFileName, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
			if err != nil {
				return errors.New(fmt.Sprintf("cannot create %s, error: %s", newFileName, err))
			}
		} else {
			return errors.New(fmt.Sprintf("cannot open %s for writing, error: %s", newFileName, err))
		}
	}
	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			log.Fatalf("Could not properly close %s, error: %s", f.Name(), err)
		}
	}(f)
	if err = png.Encode(f, img); err != nil {
		return errors.New(fmt.Sprintf("Could not encode  %s, error: %s", f.Name(), err))
	}
	return nil
}

func ClassicSharpen(img *image.Image) (*image.RGBA, error) {
	return ConvolveWithKernel(img, &ClassicSharpenKernel)
}

func SharpenTestWithAlpha(img *image.Image, alpha float64) (*image.RGBA, error) {
	return ConvolveWithKernel(img, NewTestKernelWithAlpha(alpha))
}

func ToRgba(img *image.Image) *image.RGBA {
	var result *image.RGBA
	switch iimg := (*img).(type) {
	case *image.RGBA:
		result = iimg
	case *image.NRGBA:
		bounds := (*img).Bounds()
		rgba := image.NewRGBA(image.Rect(0, 0, bounds.Dx(), bounds.Dy()))
		draw.Draw(rgba, bounds, *img, bounds.Min, draw.Src)
		result = rgba
	}
	return result
}

func ConvolveWithKernel(img *image.Image, kernel *Kernel) (*image.RGBA, error) {
	var result *image.RGBA
	var err error = nil
	switch iimg := (*img).(type) {
	case *image.RGBA:
		result, err = ConvolveRGBA(
			iimg,
			kernel,
			image.Point{X: 1, Y: 1},
		)
	case *image.NRGBA:
		bounds := (*img).Bounds()
		rgba := image.NewRGBA(image.Rect(0, 0, bounds.Dx(), bounds.Dy()))
		draw.Draw(rgba, bounds, *img, bounds.Min, draw.Src)
		result, err = ConvolveRGBA(
			rgba,
			kernel,
			image.Point{X: 1, Y: 1},
		)
	}
	if err != nil {
		return nil, err
	}
	return result, nil
}

func Basename(file, ext string) string {
	file = filepath.Base(file)
	var extension = filepath.Ext(file)
	return fmt.Sprintf("%s.%s", file[0:len(file)-len(extension)], ext)
}
